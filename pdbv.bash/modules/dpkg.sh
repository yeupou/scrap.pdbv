# (c) 2002, Mathieu Roy <yeupou@gnu.org>
# dpkg.sh: this file is part of package_db_view
#
#   all dpkg commands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: dpkg.sh,v 1.20 2002/11/27 17:54:13 yeupou Exp $

echo "The purpose of this file is not being executed alone"
exit 


# init values for dpkg
TS_GROUP="$TS_GROUP_DPKG"
PACKAGE_INFO_BASEURL="http://packages.debian.org/"

# return the dpkg version
function package_db_view_packagemanager_version {
    dpkg --version | head -n 1 | sed s/"\."$//g
}

# return the packages list
function package_db_view_packages { 
    # First proposal:
    #   dpkg --list | sed -ne '1,5!s%^..  %%p' | cut -f1 -d' '
    # Second proposal:
    # faster solution without using dpkg
    # Third proposal, the second one modified to put also version
    # --color=never is capital!
    for WORKING_PACKAGE in `(cd $DPKG_DIR/info; ls --color=never *.list) | sed -e 's/\.list//'`; do
	echo "$WORKING_PACKAGE"_`package_db_view_version`
    done
}

function package_db_view_packages_sorted {
    # it's already sorted
    package_db_view_packages
}

# return standard package info
function package_db_view_info { 
    # dpkg --status $WORKING_PACKAGE 
    # a lot faster:
    # We need to remove the version for the working package name
    cat $DPKG_DIR/status | sed -ne "/^Package: `package_db_view_basename`$/,/^$/p"
}


# return all files in a package
function package_db_view_list { 
    # dpkg --listfiles $WORKING_PACKAGE
    # a lot faster:
    cat $DPKG_DIR/info/`package_db_view_basename`.list
}

# return package name without the version
function package_db_view_basename {
    echo $WORKING_PACKAGE | sed s/\_.*$//g
}

# return group to which one the package belongs
# it can be usefull for list by criterion
function package_db_view_group {
    cat $DPKG_DIR/status | sed -ne "/^Package: `package_db_view_basename`$/,/^$/p" | grep ^"Section:" | cut -b 10-200 | tr "/" "-" | tr " " "_"
}

# return version of one package
# currently vital for dpkg listing
function package_db_view_version {
    # In this case, normally, we can consider that the WORKING_PACKAGE does
    # not content the version.
    cat $DPKG_DIR/status | sed -ne "/^Package: $WORKING_PACKAGE$/,/^$/p" | grep ^"Version:" | cut -b 10-200
}

# return installation date (YYYY Month) of the package
# it can be usefull for list by criterion
function package_db_view_date {
    WORKING_PACKAGE=`package_db_view_basename`
    # The option --time-style= exists only on the recent LS
    # But nicely, there is recent ls in the coreutils debian package for
    # sid. Too bad, not in woody. We need to test ls version at runtime to
    # get package for sarge/sid working in woody, but without this option.
    YEAR=`ls $DPKG_DIR/info/$WORKING_PACKAGE.list --full-time --time-style=+%Y | awk '{print $6}'`
    MONTH=`ls $DPKG_DIR/info/$WORKING_PACKAGE.list --full-time --time-style=+%m | awk '{print $6}'`
    echo $YEAR"_"$MONTH
}

# end