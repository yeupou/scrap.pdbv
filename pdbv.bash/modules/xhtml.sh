# (c) 2002, Mathieu Roy <yeupou@gnu.org>
# xhtml.sh: this file is part of package_db_view
#
#   XHTML (and HTML) rendering
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: xhtml.sh,v 1.23 2003/01/06 18:37:58 yeupou Exp $

echo "The purpose of this file is not being executed alone"
exit 

# initialize some vars depending on the choice html/xhtml
function package_db_view_xhtml_init {
    export XHTML_DOCTYPE="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">"
    export XHTML_LONETAGCLOSING=" />"  
    export XHTML_LONETAGCLOSING_FORSED=" \/>"  
}

function package_db_view_html_init {
    export XHTML_DOCTYPE="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"
    export XHTML_LONETAGCLOSING=">"
    export XHTML_LONETAGCLOSING_FORSED=$XHTML_LONETAGCLOSING   
}

# generate the package list's frame
function package_db_view_xhtml_make_header {
    echo "$XHTML_DOCTYPE"
    echo "<html>"
    echo "<head>"
    echo "<title>$TITLE</title>"
    echo "<meta name=\"Generator\" content=\"$SNAME $SVER with $SHELL at `date`\"$XHTML_LONETAGCLOSING"
    echo "</head>"
    if [ ! $NOBODY ]; then
	echo "<body>"
    else
	unset NOBODY
    fi
}

function package_db_view_xhtml_make_footer {
    echo "<p><font size=\"-2\"><i><a href=\"$SURL\">$SNAME-$SVER</a> `date`</i></font></p>"
    echo "</body>"
    echo "</html>"
}


function package_db_view_xhtml_make_listing_frame {
    package_db_view_xhtml_make_header
    echo "<p>"
    echo "$TS_SORT_BY"
    echo "<a href=\"list.html\" target=\"list\">$TS_PACKAGE</a>"
    if [ "$LISTING" == "group" ] || [ "$LISTING" == "all" ]; then
	echo " | "
	echo "<a href=\"list_bygroup.html\" target=\"list\">$TS_GROUP</a>"
    fi
    if [ "$LISTING" == "date" ] || [ "$LISTING" == "all" ]; then
	echo " | "
	echo "<a href=\"list_bydate.html\" target=\"list\">$TS_DATE</a>"
    fi
    echo "</p>"
    package_db_view_xhtml_make_footer
}


function package_db_view_xhtml_make_list_frame {
    TITLE="$TS_PACKAGE_LIST"
    package_db_view_xhtml_make_header
    PKG_NUMBER=0
    for package in `package_db_view_packages_sorted` ; do 
	PKG_NUMBER=`expr $PKG_NUMBER + 1`
	echo $PKG_NUMBER". "
	echo "<a href=\"package/$package.html\" target=\"package\">$package</a><br$XHTML_LONETAGCLOSING"
    done
    package_db_view_xhtml_make_footer
}

function package_db_view_xhtml_make_list_bycriterion_frame {
    # It is important to start this command only if a $criterion as been
    # set.
    # Basically, we should have something like the following before 
    # $criterion=package_db_view_group
    TITLE="$TS_PACKAGE_LIST"
    package_db_view_xhtml_make_header
    PKG_NUMBER=0
  
    # We store the list of each group possible in a file
    # finally we print to STDOUT the whole thing
    # Normally we should be able to access /tmp
    rm -rf /tmp/$SNAME$SVER.tmp 2>&1
    mkdir /tmp/$SNAME$SVER.tmp 2>&1
    for package in `package_db_view_packages_sorted` ; do 
	WORKING_PACKAGE="$package"
	PKG_NUMBER=`expr $PKG_NUMBER + 1`
	echo "&nbsp;&nbsp;&nbsp;"$PKG_NUMBER". "  >> /tmp/$SNAME$SVER.tmp/`$criterion`.group
	echo "<a href=\"package/$package.html\" target=\"package\">$package</a><br$XHTML_LONETAGCLOSING" >> /tmp/$SNAME$SVER.tmp/`$criterion`.group
	#if [ $DEBUG ]; then echo "criterion: `$criterion`"; fi
    done
    for output in /tmp/$SNAME$SVER.tmp/*.group ; do
	echo "<p><b>`basename $output .group | tr "-" "/" | tr "_" " " `:</b><br$XHTML_LONETAGCLOSING"
	cat $output
	echo "</p>"
    done
    package_db_view_xhtml_make_footer
    rm -rf /tmp/$SNAME$SVER.tmp  2>&1
}

function package_db_view_xhtml_make_item_frame {
    TITLE="$WORKING_PACKAGE"
    package_db_view_xhtml_make_header
    echo "<h2>"
    echo $"Details for "
    package_db_view_xhtml_make_package_pointer `package_db_view_basename`
    echo "</h2><br$XHTML_LONETAGCLOSING"
    
    if [ $PDBV_TYPE == dpkg ] ; then
	package_db_view_info |  sed s/"Package.*:"/"$XNAME"/ | sed s/"Version.*:"/"$XVERSION"/ | sed s/"Status.*:"/"$XSTATUS"/ | sed s/"Priority.*:"/"$XPRIORITY"/ | sed s/"Section.*:"/"$XSECTION"/ | sed s/"Installed-Size.*:"/"$XINSTALLEDSIZE"/ | sed s/"Maintainer.*:"/"$XPACKAGER"/ | sed s/"Depends.*:"/"$XDEPENDS"/ |  sed s/"Description.*:"/"$XDESCRIPTION"/ | sed s/"Conflicts.*:"/"$XCONFLICTS"/  | sed s/"Conffiles.*:"/"$XCONFFILES"/ | sed s/$/"<br$XHTML_LONETAGCLOSING_FORSED"/ 
    elif [ $PDBV_TYPE == rpm ] ; then
	package_db_view_info | sed s/$/"<br$XHTML_LONETAGCLOSING_FORSED"/ 
	echo "<hr$XHTML_LONETAGCLOSING"
	echo "$XREQUIRES"
	echo "<br$XHTML_LONETAGCLOSING"
	package_db_view_requires | sed s/$/"<br$XHTML_LONETAGCLOSING_FORSED"/
	echo "<hr$XHTML_LONETAGCLOSING"
	echo "$XPROVIDES" 
	echo "<br$XHTML_LONETAGCLOSING"
	package_db_view_provides | sed s/$/"<br$XHTML_LONETAGCLOSING_FORSED"/
    else
	echo $"$SNAME: internal error: unable to understand what is the database type"
	exit
    fi
    echo "<hr$XHTML_LONETAGCLOSING"
    echo $"Files:"
    echo "<br$XHTML_LONETAGCLOSING"
    package_db_view_list | sed s/$/"<br$XHTML_LONETAGCLOSING_FORSED"/
    package_db_view_xhtml_make_footer
}

function package_db_view_xhtml_make_info_frame {
    TITLE="$TS_INFO"
    package_db_view_xhtml_make_header
    package_db_view_sysinfo_distro
    echo "<br$XHTML_LONETAGCLOSING"
    package_db_view_sysinfo_uname_kernel
    echo "<br$XHTML_LONETAGCLOSING"
    package_db_view_sysinfo_uname_cpu
    echo "<br$XHTML_LONETAGCLOSING"
    package_db_view_sysinfo_mem 
    echo "<br$XHTML_LONETAGCLOSING"
    package_db_view_sysinfo_glibc
    echo "<br$XHTML_LONETAGCLOSING"

    echo "<p>"
    TMPSHVERSION=`package_db_view_sysinfo_shell`
    TMPDBVERSION=`package_db_view_packagemanager_version`
    echo $"This is the output of the $HOSTNAME's $TMPDBVERSION database, generated by $TMPSHVERSION running $SNAME version $SVER on `date`."
    unset TMPSHVERSION TMPDBVERSION
    echo "</p>"
    package_db_view_xhtml_make_footer
}

function package_db_view_xhtml_make_index_frame {
    TITLE="$PDBV_TYPE@$HOSTNAME"
    NOBODY=1 package_db_view_xhtml_make_header
    echo "<frameset framespacing=\"0\" border=\"false\" frameborder=\"0\" cols=\"35%,65%\">"
    if [ $LISTING ]; then 
	echo "      <frameset framespacing=\"0\" border=\"false\" frameborder=\"0\" rows=\"10%,90%\">"
	echo "          <frame name=\"listing\" src=\"listing.html\" scrolling=\"auto\"$XHTML_LONETAGCLOSING"
	echo "          <frame name=\"list\" src=\"list.html\" scrolling=\"auto\"$XHTML_LONETAGCLOSING"
	echo "      </frameset>"
    else
	echo "   <frame name=\"list\" src=\"list.html\" scrolling=\"auto\"$XHTML_LONETAGCLOSING"
    fi
    echo "  <frame name=\"package\" src=\"info.html\" scrolling=\"auto\"$XHTML_LONETAGCLOSING"
    echo "</frameset>"
    echo "<body>"
    package_db_view_xhtml_make_footer
}


function package_db_view_xhtml_make_package_pointer {
    # syntax: package_db_view_xhtml_make package
    echo "<a href=\""$PACKAGE_INFO_BASEURL"$1\">$1</a>"
}


# end