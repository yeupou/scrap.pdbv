# (c) 2002, Mathieu Roy <yeupou@gnu.org>
# translationstring.sh: this file is part of package_db_view
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.gnu.org/projects/pdbv
#  send comments at <pdbv-dev@mail.freesoftware.fsf.org>
#
#  $Id: translationstrings.sh,v 1.2 2002/10/18 15:19:21 yeupou Exp $

#  Here we store strings that will be used later
#  We do not want to translated string for each output type
#  If you add entries, please, do a | sort

# General usage
TS_DATE=`echo $"Date"`
TS_GROUP_RPM=`echo $"Group"`
TS_GROUP_DPKG=`echo $"Section"`
TS_INFO=`echo $"Info"`
TS_PACKAGE=`echo $"Package"`
TS_PACKAGE_LIST=`echo $"Package List"`
TS_SORT_BY=`echo $"Sort by"`

# Package details
XARCH=`echo $"Architecture:"`
XBUILD_DATE=`echo $"Build Date:"`
XBUILD_HOST=`echo $"Build Host:"`
XCONFFILES=`echo $"Conffiles:"`
XCONFLICTS=`echo $"Conflicts:"`
XDEPENDS=`echo $"Depends:"`
XDESCRIPTION=`echo $"Description:"`
XGROUP=`echo $"$TS_GROUP_RPM:"`
XINSTALL_DATE=`echo $"Install date:"`
XINSTALLEDSIZE=`echo $"Installed-Size:"`
XNAME=`echo $"Name:"`
XPACKAGER=`echo $"Packager:"`
XPRIORITY=`echo $"Priority:"`
XPROVIDES=`echo $"Provides:"`
XRELEASE=`echo $"Release:"`
XREQUIRES=`echo $"Requires:"`
XSECTION=`echo $"$TS_GROUP_DPKG:"`
XSIZE=`echo $"Size:"`
XSTATUS=`echo $"Status:"`
XSUMMARY=`echo $"Summary:"`
XURL=`echo $"Url:"`
XVENDOR=`echo $"Vendor:"`
XVERSION=`echo $"Version:"`

