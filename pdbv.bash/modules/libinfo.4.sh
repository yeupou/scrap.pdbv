#!/bin/sh
#
# (c) Mathieu Roy <yeupou@gnu.org>
# libinfo.4.sh: part of tui-sh
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# functions	This file contains functions to be used by most or all
#		shell scripts included in tui-sh.
#
#  take a look at http://www.freesoftware.fsf.org/tui-sh
#  send comments at <tui-sh-dev@mail.freesoftware.fsf.org>
#
# $Id:
#
# This file was modified for pdbv. Anyway, tui-sh doesnt ship this
# version anymore.


function Slibinfo_help {
        echo $"Usage : $SNAME $SUSAGE"
	echo "$SDESC"
	echo ""
	if [ "$SHELP_00" != "" ]; then echo "$SHELP_00" ; fi
	if [ "$SHELP_01" != "" ]; then echo "$SHELP_01" ; fi
	if [ "$SHELP_02" != "" ]; then echo "$SHELP_02" ; fi
	if [ "$SHELP_03" != "" ]; then echo "$SHELP_03" ; fi
	if [ "$SHELP_04" != "" ]; then echo "$SHELP_04" ; fi
	if [ "$SHELP_05" != "" ]; then echo "$SHELP_05" ; fi
	if [ "$SHELP_06" != "" ]; then echo "$SHELP_06" ; fi
	if [ "$SHELP_07" != "" ]; then echo "$SHELP_07" ; fi
	if [ "$SHELP_08" != "" ]; then echo "$SHELP_08" ; fi
	if [ "$SHELP_09" != "" ]; then echo "$SHELP_09" ; fi
	if [ "$SHELP_10" != "" ]; then echo "$SHELP_10" ; fi
	if [ "$SHELP_11" != "" ]; then echo "$SHELP_11" ; fi
	if [ "$SHELP_12" != "" ]; then echo "$SHELP_12" ; fi
	if [ "$SHELP_13" != "" ]; then echo "$SHELP_13" ; fi
	if [ "$SHELP_14" != "" ]; then echo "$SHELP_14" ; fi
	if [ "$SHELP_15" != "" ]; then echo "$SHELP_15" ; fi
	if [ "$SHELP_16" != "" ]; then echo "$SHELP_16" ; fi
	if [ "$SHELP_17" != "" ]; then echo "$SHELP_17" ; fi
	if [ "$SHELP_18" != "" ]; then echo "$SHELP_18" ; fi
	if [ "$SHELP_19" != "" ]; then echo "$SHELP_19" ; fi
        echo $"  -h, --help                 display this help and exit"
        echo $"      --version              output version information and exit"
	echo ""
#	echo $"see also : man $SNAME"
	if [ "$SREQUIRE" != "" ]; then echo $"To use this script, you need $SREQUIRE" ; fi
	echo $"Report bugs or suggestions to <$SMAIL>"
	exit
}

function Slibinfo_version {
    echo "$SNAME - $SVER"
    echo ""
    echo $"Copyright (c) $SAUTHOR <$SMAIL>"
    echo $"See AUTHORS for the full list."
    echo $"This is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
    exit
}
