# (c) 2002, Mathieu Roy <yeupou@gnu.org>
# rpm.sh: this file is part of package_db_view
#
#   all RPMS commands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: rpm.sh,v 1.8 2002/11/27 17:54:13 yeupou Exp $

echo "The purpose of this file is not being executed alone"
exit 

# init values for rpm
TS_GROUP="$TS_GROUP_RPM"
PACKAGE_INFO_BASEURL="http://www.rpmfind.net/linux/rpm2html/search.php?query="


# return the rpm version
function package_db_view_packagemanager_version {
    rpm --version | head -n 1 | sed s/"\."$//g
}

# return the packages list
function package_db_view_packages {
    rpm -qa #--qf '%{name}-%{version}-%{release}\n'
}

function package_db_view_packages_sorted { 
    package_db_view_packages | sort
}

function package_db_view_group_packages {
    rpm -q --group $WORKING_GROUP
}


# return standard package info
function package_db_view_info {
    # More complete than rpm -qi
    rpm -q $WORKING_PACKAGE --qf '%{summary}\n\n'"$XNAME"' %{name}\n'"$XVERSION"' %{version}\n'"$XRELEASE"' %{release}\n'"$XARCH"' %{arch}\n'"$XINSTALL_DATE"' %{installtime:date}\n\n'"$XBUILD_HOST"' %{buildhost}\n'"$XBUILD_DATE"' %{buildtime:date}\n'"$XPACKAGER"' %{packager}\n'"$XSIZE"' %{size}\n'"$XGROUP"' %{group}\n\n'"$XDESCRIPTION"'\n%{description}\n'"$XURL"' %{url}\n'
}

function package_db_view_group {    
    rpm -q $WORKING_PACKAGE --qf '%{GROUP}'
}


# return all files in a package
function package_db_view_list {
    # --status give a --list with more informations.
    # will change probably someday
    rpm -q --list $WORKING_PACKAGE
 }


# return package name without the version
function package_db_view_basename {
    rpm -q $WORKING_PACKAGE --qf '%{NAME}'
}

# return what the rpm provides
function package_db_view_provides {
    rpm -q --provides $WORKING_PACKAGE 
}

# return what the rpm requires
function package_db_view_requires {
    rpm -q --requires $WORKING_PACKAGE
}

# return group to which one the rpm belongs
# it can be usefull for list by criterion
function package_db_view_group {    
    rpm -q $WORKING_PACKAGE --qf '%{GROUP}' | tr "/" "-" | tr " " "_"
}

# return installation date (YYYY Month) of the rpm
# it can be usefull for list by criterion
function package_db_view_date {
    YEAR=`rpm -q $WORKING_PACKAGE --qf '%{INSTALLTIME:date}' | awk '{print $4}'`
    MONTH=`rpm -q $WORKING_PACKAGE --qf '%{INSTALLTIME:date}' | awk '{print $3}'`
    echo $YEAR"_"$MONTH
}


# end