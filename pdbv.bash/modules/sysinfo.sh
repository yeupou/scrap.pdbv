# (c) 2002, Mathieu Roy <yeupou@gnu.org>
# sysinfo.sh: this file is part of package_db_view
#
#   Any system specific values.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: sysinfo.sh,v 1.1 2002/11/27 16:55:58 yeupou Exp $

echo "The purpose of this file is not being executed alone"
exit 

# We want to know the computer  with deal with.
# We can access directly /proc/cpuinfo and this kind of files that
# give much informations. But in fact, we will use uname et caetera.
# More portable, but less informative with older uname.

# Shell version?
function package_db_view_sysinfo_shell {
    $SHELL --version | head -n 1 | sed s/"\."$//g | sed s/"\,"//g
}

# Glibc version?
function package_db_view_sysinfo_glibc {
    GLIBCTEST="getent"
    GLIBCTEST_ABSOLUTE=`which $GLIBCTEST 2> /dev/null`
    if [ -x "$GLIBCTEST_ABSOLUTE" ] ; then
	$GLIBCTEST --version | grep $GLIBCTEST | sed s/"$GLIBCTEST"//g | sed s/"("//g | sed s/")"//g | sed s/^\ *//g
    else 
	echo $"Unknown libc"
    fi
    unset GLIBCTEST GLIBCTEST_ABSOLUTE
}

# Distro version?
function package_db_view_sysinfo_distro {
    DEBIAN_REL=/etc/debian_version
    REDHAT_REL=/etc/redhat-release
    if [ -e $REDHAT_REL ] ; then
	cat $REDHAT_REL
    elif [ -e $DEBIAN_REL ] ; then
	DEBIAN_REL=`cat /etc/debian_version`
	echo $"Debian GNU release $DEBIAN_REL"
    else 
	echo $"Unknown distribution"
    fi
    unset DEBIAN_REL REDHAT_REL
}

# Kernel?
function package_db_view_sysinfo_uname_kernel {
    UNAME_MR=`uname -mr 2> /dev/null`
    UNAME_V=`uname -v 2> /dev/null`
    echo $"Kernel $UNAME_MR, compiled $UNAME_V"
    unset UNAME_MR UNAME_V
}

# Cpu?
function package_db_view_sysinfo_uname_cpu {
    UNAME_P=`uname -p 2> /dev/null`
    UNAME_I=`uname -i 2> /dev/null`
    if [ "$UNAME_I" != "" ] ; then UNAME_I="($UNAME_I)"; fi 
    echo $"Processor $UNAME_P $UNAME_I"
    unset UNAME_P UNAME_I
}

# Ram, swap?
function package_db_view_sysinfo_mem {
    # Dont know software standard similar to uname that give that
    # Anyway, it should be more portable than /proc/cpuinfo stuff.
    # Since it's just information, no big deal.
    MEMINFO=/proc/meminfo
    MEMTOTAL=`cat $MEMINFO | grep MemTotal | cut -b 10-100 | sed s/^\ *//g`
    SWAPTOTAL=`cat $MEMINFO | grep SwapTotal | cut -b 11-100 | sed s/^\ *//g`
    echo $"$MEMTOTAL RAM, $SWAPTOTAL swap"
    unset MEMINFO MEMTOTAL SWAPTOTAL
}

# end