# (c) 2002, Mathieu Roy <yeupou@gnu.org>
# core.sh: this file is part of package_db_view
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: core.sh,v 1.59 2003/04/16 20:53:51 yeupou Exp $

echo "The purpose of this file is not being executed alone"
exit 

#### Configuration ####
# General
SNAME="pdbv"
SVER="1.2.7-2"
SAUTHOR="Mathieu Roy"
SMAIL="pdbv-dev@nongnu.org"
SURL="http://savannah.nongnu.org/projects/pdbv"
SUSAGE=`echo $"[OPTION]"`
SHELP_00=`echo $"  -d, --dir=/path            output directory"`
SHELP_01=`echo $"      --html                 html output"`
SHELP_02=`echo $"      --xhtml                xhtml output (default)"`
SHELP_03=`echo $"  -l, --listing=type         type can be basic (default), all, date"`
SHELP_04=`echo $"                             and group (xhtml/html option)"`
SHELP_05=`echo $"  -f, --force                refresh the whole output without tests"`
SHELP_06=`echo $"      --nosleep              (see man page)"`
SHELP_07=" "
SHELP_08="(Note: Have you ever tried pdbv 2.x.x ?)"
SHELP_09=" "

# Default values
unset IS_THERE_NEW_PACKAGE DEBUG FORCE WORKING_DIR LISTING OUTPUT_DIR
OUTPUT=xhtml
SLEEP=1


#### Core Functions ####

function package_db_view_generate {
    # Init: 
    mkdir -p $WORKING_DIR/package
    #  init the test of orphans
    TEST=$WORKING_DIR/package/tmp.test
    touch $TEST
    
    #  Sleep to help find to deal with fast computer unless user assume
    #  his computer will handle the task without trouble.
    #  see http://mail.nongnu.org/pipermail/pdbv-dev/2002-September/000069.html
    if [ $SLEEP == 1 ]; then
	if [ $DEBUG ]; then
	    echo $"7sec sleep... your computer is too fast for find"
	fi
	sleep 7
    fi 
    # Gen a general info frame:
    $package_db_view_make_info_frame > $WORKING_DIR/info$output_ext
    # Gen each package frame:
    #
    #  * we touch files or create it in case it does not already exists
    #  * we don't create an item if it already exists
    #  * we remove files that havent been touched before the test item:
    #    no longer in the database
    #
    #  est of orphans
    if [ $DEBUG ]; then 
	ls --full-time $TEST
    fi
    #  test every package and generate, if needed
    for WORKING_PACKAGE in `package_db_view_packages` ; do
	if [ ! -e $WORKING_DIR/package/$WORKING_PACKAGE$output_ext ] || [ $FORCE ]; then
	    $package_db_view_make_item_frame > $WORKING_DIR/package/$WORKING_PACKAGE$output_ext
	    IS_THERE_NEW_PACKAGE=1
	else
	    touch $WORKING_DIR/package/$WORKING_PACKAGE$output_ext
	fi
    done
    #  seek and destroy orphans (the test file should die also)
    ORPHANS=`find $WORKING_DIR/package ! -newer $TEST -mindepth 1 -maxdepth 1`
    if [ $DEBUG ]; then 
	echo "ORPHANS: $ORPHANS"
    fi
    #  remove each orphans one by one
    #  (not in a whole bunch, to avoid "/bin/rm: Argument list too long")
    #  wonder if xargs wont do the job, but apparently same limit than for rm
    for orphan in $ORPHANS; do
	rm -f $orphan
    done
    unset ORPHANS
    # Gen the lists frames:
    #  basic list is alway generated.
    #  by default, it's the only one. It will change as soon as we find
    #  fastest "enhanced list" generation process.
    #  Delete listing not asked. They will be outdated.
    if [ $IS_THERE_NEW_PACKAGE ] || [ "$ORPHANS" != $TEST ]; then
	$package_db_view_make_list_frame > $WORKING_DIR/list$output_ext
	case "$LISTING" in
	    all)
		criterion=package_db_view_group
		$package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bygroup$output_ext
		criterion=package_db_view_date
		$package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bydate$output_ext
		;;
	    group)
		criterion=package_db_view_group
		$package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bygroup$output_ext
		rm -f $WORKING_DIR/list_bydate$output_ext 2>&1
		;;
	    date)
		criterion=package_db_view_date
		$package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bygroup$output_ext
		rm -f $WORKING_DIR/list_bygroup$output_ext 2>&1
		;;
	    basic)
		rm -f $WORKING_DIR/list_bygroup$output_ext 2>&1
		rm -f $WORKING_DIR/list_bydate$output_ext 2>&1
		unset LISTING
		;;	    
	esac
	if [ $LISTING ]; then
	    $package_db_view_make_listing_frame >  $WORKING_DIR/listing$output_ext
	fi
    else
    #  If there no installed or deleted package, we need to check if the 
    #  specified list is up-to-date.
    #  If previous list exists, do nothing (means that nothing as changed
    #  since its creation), otherwise, generate.
    #  This is not the faster way but the first one that came to my mind.
    #  Please, propose enhancement to pdbv-dev@nongnu.org
	case "$LISTING" in
	    all)		
		if [ ! -e  $WORKING_DIR/list_bygroup$output_ext ]; then
		    criterion=package_db_view_group
		    $package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bygroup$output_ext
		fi
		if [ ! -e  $WORKING_DIR/list_bydate$output_ext ]; then
		    criterion=package_db_view_date
		    $package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bydate$output_ext
		fi
		;;
	    group)
		if [ ! -e  $WORKING_DIR/list_bygroup$output_ext ]; then
		    criterion=package_db_view_group
		    $package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bygroup$output_ext	
		fi
		;;
	    date)
		if [ ! -e  $WORKING_DIR/list_bydate$output_ext ]; then
		    criterion=package_db_view_date
		    $package_db_view_make_list_bycriterion_frame > $WORKING_DIR/list_bygroup$output_ext
		fi
		;;
	esac
	$package_db_view_make_listing_frame >  $WORKING_DIR/listing$output_ext
	
    fi

    # Gen an index:
    $package_db_view_make_index_frame > $WORKING_DIR/index$output_ext
}

#### Run baby run! ####
# Get options
# If debug mode, show full debug info
if [ $DEBUG ]; then set -xv ; fi
# First, get options from the configuration file
if [ -e /etc/pdbvrc1 ] ; then
    if [ $DEBUG ]; then  echo "source /etc/pdbvrc1" ; fi
    . /etc/pdbvrc1
fi
if [ -e $HOME/.pdbvrc1 ] ; then
    if [ $DEBUG ]; then  echo "source $HOME/.pdbvrc1" ; fi
    . $HOME/.pdbvrc1
fi
# In configure file, with use OUTPUT_DIR to define WORKING_DIR
if [ $OUTPUT_DIR ] ; then
    WORKING_DIR=$OUTPUT_DIR
fi

# Second, get options from the command line
# (override previous)
GETOPT_TEMP=`getopt -o hvd:fl: --long help,version,html,xhtml,debug,dir:,force,listing:,nosleep \
     -n $SNAME -- "$@"`
eval set -- "$GETOPT_TEMP"

# Set action
while true ; do
    case "$1" in
	-h|--help) Slibinfo_help ; break ;;
	--version) Slibinfo_version ; break ;;
	--debug) DEBUG=1 ; shift ;;
	-f|--force) FORCE=1 ; shift ;; 
	-d|--dir) WORKING_DIR="$2" ; shift 2 ;; 
	-l|--listing) LISTING="$2" ; shift 2 ;;
	--xhtml) OUTPUT=xhtml ; shift ;;
	--html) OUTPUT=html ; shift ;;
	--nosleep) SLEEP=0 ; shift ;;
	*) break ;; 
    esac
done
   # Outdated:
   # Output directory  should be the last remaining arg from getopt
   #for arg do WORKING_DIR="$arg"; done

   #if [ ! -e "$WORKING_DIR" ] && [ ! $DEBUG ]; then
   #    echo $"The output directory $WORKING_DIR does not exists."
   #    echo $"Please specify an existing directory."
   #    Slibinfo_help
   #fi
   # If we are here, output_dir exist, we add to it the pdbv dir
   #WORKING_DIR="$WORKING_DIR/pdbv"

# Third, check if every required options are set.
if [ ! $WORKING_DIR ]; then
    echo $"You must choose an output directory."
    echo $"Check your pdbvrc or add a command line's option."
    Slibinfo_help
fi

# If debug mode, show options
if [ $DEBUG ]; then
    echo "DEBUG = $DEBUG"
    echo "FORCE = $FORCE"
    echo "OUTPUT = $OUTPUT"
    echo "WORKING_DIR = $WORKING_DIR"
    echo "LISTING = $LISTING"
    echo "SLEEP = $SLEEP"
fi

# Which output type ?
if [ $OUTPUT == xhtml ] ; then
    package_db_view_xhtml_init
elif [ $OUTPUT == html ]; then
    package_db_view_html_init
else
    echo $"$SNAME: Internal error: unable to understand the type of output specified"
fi
# For now, we only have xhtml and html output. So, no need to check output
# type to assign the following values.
output_ext=".html"    
package_db_view_make_header=package_db_view_xhtml_make_header
package_db_view_make_footer=package_db_view_xhtml_make_footer
package_db_view_make_listing_frame=package_db_view_xhtml_make_listing_frame
package_db_view_make_list_frame=package_db_view_xhtml_make_list_frame
package_db_view_make_list_bycriterion_frame=package_db_view_xhtml_make_list_bycriterion_frame
package_db_view_make_item_frame=package_db_view_xhtml_make_item_frame
package_db_view_make_info_frame=package_db_view_xhtml_make_info_frame
package_db_view_make_index_frame=package_db_view_xhtml_make_index_frame

# The option --time-style= exists only on the recent ls
# We need it for listing by date. If not available, override settings
# and remove date of listing to output. 
if ! ls /tmp --full-time --time-style=+%Y 2> /dev/null > /dev/null ; then
    case "$LISTING" in
	# If user asked for all outputs, only group and basic remains
	all) LISTING=group ;;
	# If user asked for date output, only basic remains
	date) LISTING=basic ;;
    esac
fi

# Finally, do the job, if we are not in debug mode.
# Debug mode permit to do '. pdbv' without killing the shell.
if [ ! $DEBUG ] || [ $FORCE ] ; then 
    # First, check if we need to create an output dir. Do not 
    # create the output dir with -p option. Assume that if base path
    # for the output does not exists, it can be a wrong path.
    if [ ! -e $WORKING_DIR ]; then
	mkdir $WORKING_DIR
    fi
    if [ ! -e $WORKING_DIR ]; then
	echo $"Unable to create $WORKING_DIR."
	echo $"Check if the parent directory exists."
	echo $"Check also its mode and ownership." 
    fi
    package_db_view_generate
fi

#### END ####
