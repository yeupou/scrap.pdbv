HISTORY:

Iniatialy, pdbv (1.x) was entirely written in bash.

Hopefully to get something faster, the 2.x is in perl. It will also
help me to learn more about perl.

I decided to only rewrite, not to rethink pdbv. So technically
speaking, there's is nothing new in the first 2.x version by
comparison with the last 1.x version.

In fact, this assertion is partially wrong: perl can do many things in
a easier and faster way, so step by step I'll try to optimize the
code. For instance, %hashes aren't available in bash while they indeed
are in perl... and it can helps a lot.


REQUIREMENTS:

use Locale::gettext;
use POSIX;
use Getopt::Long;
use Term::ANSIColor qw(:constants);
use File::stat;
use Sys::Hostname;
use FileHandle;

For Debian, the package dependancies should be correctly filled. 
For RedHat (or rpm based system), you must find (see http://rpmfind.net)
the package perl-gettext.


ABOUT RPM SUPPORT:

Some work need to be done in area.x. Normally, there are perl modules to
access RPM database directly. If you are willing to check how it can
be used by pdbv, write a mail to <pdbv-dev@nongnu.org>
