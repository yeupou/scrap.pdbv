# (c) 2003-2005 Mathieu Roy <yeupou@gnu.org>
# initvars.pl this file is part of package_db_view
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://gna.org/projects/pdbv
#  send comments at <pdbv-dev@gna.org>
#
#  $Id: initvars.pl,v 1.33 2005/02/23 10:42:20 yeupou Exp $

# Some vars used all alongs must be initialized since the start.
# It's easier to deal with this way, as modules insertion order may change.

# Predefined values
our $sname = "pdbv";
our $sver = "2.0.10";
our $sauthor = "Mathieu Roy";
our $smail = "pdbv-dev\@gna.org";
our $surl = "http://gna.org/projects/pdbv";

our $confdir = "/etc/pdbv";

our $sleep = "1";
our $does_new_package_exist = "0";
our $does_orphan_exist = "0";

our $output;
our $output_ext;

our $tag_item_title_open;
our $tag_item_title_close;
our $tag_line_break;

# Command lines args and configuration file options
our $cron;
our $cron_lang;
our $debug;
our $force;
our $listing;
our $output_dir;
our $working_dir;
our $light;
our $css;
our $working_dir_css;

our $getopt;
our $arg_help;
our $arg_version;
our $arg_cron;
our $arg_xhtml;

# Define epoch date of today
our $epoch_start = time;

# Translation of strings included in modules rpm/dpkg
our $sbst_unable_to_open = gettext("Unable to open ");

# Popcon default location
our $popcon_file = "/var/log/popularity-contest";

# Pure data, related to the database
our $package_info_baseurl;
our $package_installedsize_sum;
our $package_popularity_activated;

our $package_explanationurl_priority;

our %section_packages_sum;
tie %section_packages_sum, "Tie::IxHash";
our %usage_packages_sum;
tie %usage_packages_sum, "Tie::IxHash";

our %package_conffiles;
our %package_configversion;
our %package_conflicts;
our %package_depends;
our %package_description_short;
our %package_description;
our %package_enhances;
our %package_essential;
our %package_installedsize;
our %package_installdate;
our %package_license;
our %package_maintainer;
our %package_origin;
our %package_popularity;
our %package_priority;
our %package_provides;
our %package_recommends;
our %package_replaces;
our %package_section;
our %package_source;
our %package_status;
our %package_suggests;
our %package_version;
our %package_website;

