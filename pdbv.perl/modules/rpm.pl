# (c) 2002-2003 Mathieu Roy <yeupou@gnu.org>
# rpm.pl this file is part of package_db_view
#
#   all rpm commands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: rpm.pl,v 1.7 2003/11/04 20:03:59 yeupou Exp $

print "The purpose of this file is not being executed alone";
exit;

# IIRC, perl modules to access RPM database exists.
# It should be a lot faster.
# Send a mail at <pdbv-dev@nongnu.org> for proposal on this topic.

$package_info_baseurl = "http://www.rpmfind.net/linux/rpm2html/search.php?query=";
$package_installedsize_sum;
our @package_list;

sub PdbvPackageInit {
    # First, get one-line information
    my $rpmopt = "-qa --qf \'%{NAME}##%{VERSION}-%{RELEASE}##%{INSTALLTIME:date}##%{SIZE}##%{LICENSE}##%{PACKAGER}##%{GROUP}##%{SOURCERPM}##%{URL}
\'";

    open(STATUS, "rpm $rpmopt |");    
    while (<STATUS>) {
	# Figure out the package name
	my ($pack, $version) = split("##", $_);
	push(@package_list, $pack) if $pack && $version;
	next unless $pack && $version;

	my ($donotcare);

	($donotcare,
	 $package_version{$pack},
	 $package_installdate{$pack},
	 $package_installedsize{$pack},
	 $package_license{$pack},
	 $package_maintainer{$pack},
	 $package_section{$pack},
	 $package_source{$pack},
	 $package_website{$pack}) = split("##", $_); 

        # Misc formating stuff
	$package_installdate{$pack} = PdbvDate($package_installdate{$pack});

        # Whole database installed-size
	$package_installedsize_sum = ($package_installedsize{$pack} + $package_installedsize_sum);

	# Get more-than-one-line data
	$package_description{$pack} = PdbvQuery($pack, "--qf '%{DESCRIPTION}'");
	$package_depends{$pack} = PdbvQuery($pack, "--requires");
	$package_provides{$pack} = PdbvQuery($pack, "--provides");
    }    
    close(STATUS);
    @package_list = sort(@package_list);
}

# return the rpm version
sub PdbvPackagemanagerVersion {
    my $ret = `rpm --version | head -n 1`;
    chomp($ret);
    $ret =~ s/\.$//;
    return $ret;
}

sub PdbvPackages {
    return @package_list;
}

sub PdbvPackagesSorted {
    return @package_list;
}

# return package name without the version
sub PdbvBasename {
    my $ret = `rpm -q "$_[0]" --qf '%{NAME}'`;
    return $ret;
}


# return all files in a package
sub PdbvList {
    # --status give a --list with more informations.
    # will change probably someday
    my $ret = `rpm -q --list "$_[0]"`;
    return $ret;
}


# Specific query subs
# For data that does not fit one line, that miserably broke things.
# (You're are welcome to provide patches)
sub PdbvQuery {
    # For data that does not fit one line, that miserably broke things.
    # (You're are welcome to provide patches)
    my $ret = `rpm -q "$_[0]" $_[1]`;
    return $ret;
}

# return installation date (YYYY Month) of the rpm
# it can be usefull for list by criterion
sub PdbvDate { 
    my ($dayname, $day, $month, $year) = split(" ", $_[0]);
    return $year." ".$month;
}

# end
