# (c) 2002-2005 Mathieu Roy <yeupou@gnu.org>
# xhtml.pl: this file is part of package_db_view
#
#   XHTML (and HTML) rendering
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: xhtml.pl,v 1.30 2005/02/23 10:42:20 yeupou Exp $

print "The purpose of this file is not being executed alone";
exit;

sub PdbvXhtmlInit {
    $working_dir_css = "$working_dir/style.css" unless $working_dir_css;
    $output = "xhtml" unless $output;
    $output_ext = ".html" unless $output_ext;

    $tag_item_title_open = "<font class=\"itemtitle\">";
    $tag_item_title_close = "</font>";
    $tag_line_break = "<br />";
}

# To save memory, we do print instead of storing data in vars (as pdbv.bash)
#
# It means that these commands must be used with an opened handle
# specified as arg1.

sub PdbvXhtmlPrintHeader {
    # arg1 = $handle
    # arg2 = title
    # arg3 = no <body>
    # arg4 = css relative path
    my $handle = STDOUT;
    my $title = "pdbv";
    my $body = "<body><div class=\"content\">";
    my $csspath;
    $handle = $_[0] if $_[0];
    $title = $_[1] if $_[1];
    $body = "" if $_[2];
    $csspath = $_[3] if $_[3];

    print $handle "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">
<html>
<head>
  <title>".$title."</title>
  <meta name=\"Generator\" content=\"".$sname." ".$sver."\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"".$csspath."style.css\" />
</head>
".$body."\n";
}

sub PdbvXhtmlPrintFooter {
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];
    my $date = strftime "%c", localtime;

    print $handle "</div><p class=\"footer\"><a href=\"".$surl."\">".$sname."-".$sver."</a> ".$date."</p>
</body>
</html>\n";
}


sub PdbvXhtmlPrintListingFrame {
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];

    PdbvXhtmlPrintHeader($handle);
    print $handle "<p>";
    print $handle gettext("Sort by");
    print $handle " <a href=\"list".$output_ext."\" target=\"list\">".gettext("Package")."</a>";
    if ($listing) {
	print $handle " | <a href=\"list_bygroup".$output_ext."\" target=\"list\">".gettext("Section")."</a>";
	print $handle " | <a href=\"list_bydate".$output_ext."\" target=\"list\">".gettext("Date")."</a>";	
	print $handle " | <a href=\"list_byusage".$output_ext."\" target=\"list\">".gettext("Usage")."</a>" if $package_popularity_activated;
	# FIXME: Need to figure out to do that cleanly.
	#print $handle " | <a href=\"list_bysize".$output_ext."\" target=\"list\">".gettext("Size")."</a>";
    }
    print $handle "</p>";
    PdbvXhtmlPrintFooter($handle);
}

sub PdbvXhtmlPrintListFrame {
    my $handle = $_[0] if $_[0];
    my $criterion = $_[1] if $_[1];
    my $pkg_number = 0;

    PdbvXhtmlPrintHeader($handle, gettext("Package List"));
    unless ($criterion) {
	foreach my $pack (PdbvPackagesSorted()) {
	    $pkg_number++;
	    print $handle $pkg_number.". <a href=\"package/".$pack."_".$package_version{$pack}.$output_ext."\" target=\"package\">".$pack." ".$package_version{$pack}."</a>".$tag_line_break."\n";
	} 
    } else {
	my %seen_before;
	my @crit_list;
	my %crit_data;
	while(my ($key, $value) = each(%$criterion)) {
	    unless ($seen_before{$value}) {
		push(@crit_list, $value);
		$seen_before{$value} = 1;
	    }
	}

	@crit_list = sort(@crit_list);
	@crit_list = reverse(@crit_list) if $criterion eq "package_installdate";
	foreach my $crit (@crit_list) {
	    if ($criterion eq "package_installdate") {
		print $handle "<a name=\"".$crit."\" href=\"#".$crit."\" class=\"bold\">".PdbvXhtmlMakeDate($crit)."</a>".$tag_line_break;
	    } else {
		print $handle "<a name=\"".$crit."\" href=\"#".$crit."\" class=\"bold\">".$crit."</a>".$tag_line_break;
	    }
	    my $pkg_number = 0;
	    
	    foreach my $pack (PdbvPackagesSorted()) {
		if ($$criterion{$pack} eq $crit) {
		    $pkg_number++;
		    print $handle $pkg_number.". <a href=\"package/".$pack."_".$package_version{$pack}.$output_ext."\" target=\"package\">".$pack." ".$package_version{$pack}."</a>".$tag_line_break."\n";
		}
	    }
	    print $handle $tag_line_break;

	    # Store the sum of packages for each section
	    $section_packages_sum{$crit} = $pkg_number if $criterion eq "package_section";
	    $usage_packages_sum{$crit} = $pkg_number if $criterion eq "package_popularity";
	}
	
	
	
    }
    PdbvXhtmlPrintFooter($handle);
}


sub PdbvXhtmlPrintItemFrame {
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];
    my $ret;
    my $pack = $_[1];

    PdbvXhtmlPrintHeader($handle, $pack, 0, "../");

    # Title
    print $handle "<h2 class=\"title\">";
    print $handle gettext("Details for ");
    PdbvXhtmlPrintPackagePointer($handle, $pack);
    print $handle "</h2>";
    print $handle "<h3 class=\"itemtitle\">".$package_description_short{$pack}."</h3>"
	if $package_description_short{$pack};
    
    # General information about the package
    # (the "". is here to avoid a warning "Bareword missing")
    print $handle "".PdbvXhtmlTIT(gettext("Package:"))." ".$pack.$tag_line_break;
    print $handle "".PdbvXhtmlTIT(gettext("Version:"))." ".$package_version{$pack}.$tag_line_break
	if $package_version{$pack};

    print $handle "".PdbvXhtmlTIT(gettext("Section:"))." ".$package_section{$pack}.$tag_line_break
	if $package_section{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Priority:"))." ".PdbvXhtmlMakePriority($package_priority{$pack}).$tag_line_break
	if $package_priority{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Essential:"))." ".$package_essential{$pack}.$tag_line_break
	if $package_essential{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Status:"))." ".$package_status{$pack}.$tag_line_break
	if $package_status{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Install-Date:"))." ".PdbvXhtmlMakeDate($package_installdate{$pack}).$tag_line_break
	if $package_installdate{$pack};

    print $handle $tag_line_break;

    # Dependancies information about the package
    print $handle "".PdbvXhtmlTIT(gettext("Depends:"))." ".PdbvXhtmlMakeRelationLinks($package_depends{$pack}).$tag_line_break
	if $package_depends{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Conflicts:"))." ".PdbvXhtmlMakeRelationLinks($package_conflicts{$pack}, "conflicts").$tag_line_break
	if $package_conflicts{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Enhances:"))." ".PdbvXhtmlMakeRelationLinks($package_enhances{$pack}).$tag_line_break
	if $package_enhances{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Provides:"))." ".PdbvXhtmlMakeRelationLinks($package_provides{$pack}).$tag_line_break
	if $package_provides{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Recommends:"))." ".PdbvXhtmlMakeRelationLinks($package_recommends{$pack}).$tag_line_break
	if $package_recommends{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Suggests:"))." ".PdbvXhtmlMakeRelationLinks($package_suggests{$pack}).$tag_line_break
	if $package_suggests{$pack};

    print $handle $tag_line_break;

    # Origin and big description
    print $handle "".PdbvXhtmlTIT(gettext("Maintainer:"))." ".PdbvXhtmlMakeLinks($package_maintainer{$pack}).$tag_line_break
	if $package_maintainer{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Origin:"))." ".$package_origin{$pack}.$tag_line_break
	if $package_origin{$pack};
    if ($package_source{$pack}) {
	print $handle "".PdbvXhtmlTIT(gettext("Source:"))." ";
	PdbvXhtmlPrintPackagePointer($handle, $package_source{$pack});
	print $handle $tag_line_break;
    }
    print $handle "".PdbvXhtmlTIT(gettext("License:"))." ".$package_license{$pack}.$tag_line_break
	if $package_license{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Website:"))." ".PdbvXhtmlMakeLinks($package_website{$pack}).$tag_line_break
	if $package_website{$pack};
    print $handle "".PdbvXhtmlTIT(gettext("Description:"))." ".PdbvXhtmlMakeLinebreak(PdbvXhtmlMakeLinks($package_description{$pack}))
	if $package_description{$pack};
    
    
    # Files on the Harddisk
    if ($package_installedsize{$pack}) {
	print $handle "".PdbvXhtmlTIT(gettext("Installed-Size:"));
	PdbvXhtmlPrintSizeBox($handle, $package_installedsize{$pack});
	print $handle $tag_line_break;
    }

    # Usage of the package, via popcon.
    if ($package_popularity_activated) {
	print $handle "".PdbvXhtmlTIT(gettext("Usage:"));
	PdbvXhtmlPrintPopularityBox($handle, $package_popularity{$pack});
	print $handle $tag_line_break;
    }
    
    print $handle $tag_line_break;

    print $handle "<font class=\"itemtitle\">";
    print $handle gettext("Files:");
    print $handle "</font><br />";
    my @raw = split /^/, PdbvList($pack);
    foreach my $line (@raw) {
	$line =~ s/$/<br \/>/;
	print $handle $line;
    }
    print $handle $ret;

    PdbvXhtmlPrintFooter($handle);
}

sub PdbvXhtmlPrintInfoFrame {    
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];

    PdbvXhtmlPrintHeader($handle, gettext("Information"));
    
    print $handle "<p>";
    print $handle sprintf(gettext("This is the output of %s version %s, running on %s. "), "<a href=\"$surl\">$sname</a>", $sver, hostname());
    print $handle sprintf(gettext("It was generated in %s."), PdbvExecutionTime());
    print $handle "</p>";
    

    print $handle "<h2>".gettext("Packages Database Information")."</h2>";
    print $handle "<p>";
    print $handle sprintf(gettext("There are %s packages in the database."), scalar(@package_list));
    if ($package_installedsize_sum) {
	print $handle " ".sprintf(gettext("Installed packages take around %s of disk space."), PdbvXhtmlMakeSize($package_installedsize_sum));
    }
    print $handle "</p>";
    # For each section, show the number of packages found (active only if
    # section listing was made or if there's cached information about that
    my $ret;
    while(my($section,$sum) = each(%section_packages_sum)) {
	$ret .= " <a href=\"list_bygroup.html#$section\" target=\"list\">".sprintf(gettext("%s in %s"), $sum, $section)."</a>," if $section;
    }
    if ($ret) {
	chop($ret);
	print $handle "<p>".gettext("Packages were found in the following sections:")." $ret.</p>";
	open(FOUNDINSECTIONCACHE, "> $working_dir/foundinsections");
	print FOUNDINSECTIONCACHE "<p>".gettext("Packages were found in the following sections:")." $ret.</p>";

    } elsif (-e $working_dir."/foundinsections") {
	open(FOUNDINSECTIONCACHE, "< $working_dir/foundinsections");
	while (<FOUNDINSECTIONCACHE>) {
	    print $handle $_;
	}
	close(FOUNDINSECTIONCACHE);
    }

    # For each usage , show the number of packages found (active only if
    # usage listing was made or if there's cached information about that
    $ret = "";
    if ($package_popularity_activated) {
	while(my($usage,$sum) = each(%usage_packages_sum)) {
	    $ret .= " <a href=\"list_byusage.html#$usage\" target=\"list\">$sum $usage</a>," if $usage;
	}
	if ($ret) {
	    chop($ret);
	    print $handle "<p>".gettext("Packages usage appears as following:")." $ret.</p>";
	    open(FOUNDINUSAGECACHE, "> $working_dir/foundinusage");
	    print FOUNDINUSAGECACHE "<p>".gettext("Packages usage appears as following:")." $ret.</p>";
	    
	} elsif (-e $working_dir."/foundinusage") {
	    open(FOUNDINUSAGECACHE, "< $working_dir/foundinusage");
	    while (<FOUNDINUSAGECACHE>) {
		print $handle $_;
	    }
	    close(FOUNDINUSAGECACHE);
	}	
    }

    print $handle "<h2>".gettext("System Information")."</h2>";
    print $handle "<p>";
    print $handle sprintf(gettext("The system appears to be running %s along with the %s."), PdbvSysinfoGetDistro(), PdbvSysinfoGetUnameKernel())."</p><p>";
    print $handle sprintf(gettext("It is %s with %s."), PdbvSysinfoGetUnameCpu(), PdbvSysinfoGetFree());
    print $handle "</p>";

    PdbvXhtmlPrintFooter($handle);
}

sub PdbvXhtmlPrintIndexFrame {    
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];

    PdbvXhtmlPrintHeader($handle, "$pdbv_type\@".hostname(), 1);
    print $handle "<frameset framespacing=\"0\" border=\"false\" frameborder=\"0\" cols=\"35%,65%\">";
    if ($listing) {
	print $handle "      <frameset framespacing=\"0\" border=\"false\" frameborder=\"0\" rows=\"10%,90%\">"; 
	print $handle "          <frame name=\"listing\" src=\"listing.html\" scrolling=\"auto\" />";
	print $handle "          <frame name=\"list\" src=\"list.html\" scrolling=\"auto\" />";
	print $handle "      </frameset>";
    } else {
	print $handle "   <frame name=\"list\" src=\"list.html\" scrolling=\"auto\" />";
    }
    print $handle "  <frame name=\"package\" src=\"info.html\" scrolling=\"auto\" />";
    print $handle "</frameset>";
    print $handle "<body>";
    PdbvXhtmlPrintFooter($handle);
}

sub PdbvXhtmlPrintPackagePointer {    
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];

    print $handle "<a href=\"".$package_info_baseurl."".PdbvBasename($_[1])."\">".PdbvBasename($_[1])."</a>";
}

sub PdbvXhtmlTagItemTitle {    
    return $tag_item_title_open.$_[0].$tag_item_title_close;
}
sub PdbvXhtmlTIT {    
    return PdbvXhtmlTagItemTitle($_[0]);
}

sub PdbvXhtmlMakeLinks { 
    my $data = $_[0];

    # htmlize < and >
    $data =~ s/\<(.*)\>/\&lt\;\1\&gt\;/;

    unless ($light) {
	my $ltrs = '\w';
	my $gunk = '/#~:.?+=&%@!\-}';
	my $punc = '.:?\-';
	
	# build hrefs
	$data =~ s{\b( (http|ftp|https) : [${ltrs}${gunk}${punc}] +?)(?= [$punc]* [^${ltrs}${gunk}${punc}] | $ )}{<a href="$1">$1</a>}igox;
	# build mailtos
	$data =~ s{(([a-z0-9_]|\-|\.)+@([^[:space:]&><]*)([[:alnum:]-]))}{<a href=\"mailto:$1\">$1</a>}igox;
    }

    return $data;
}

sub PdbvXhtmlMakeLinebreak {
    my $data = $_[0];
    # build line breaks
    $data =~ s/$/$tag_line_break/gm;
    return $data;
}

sub PdbvXhtmlMakeRelationLinks {
    my $ret;
    if ($light) {
	$ret = $_[0];
    } else {

	# For now, this function only check dependancies over package, without
	# taking care about version.
	my @data = split(", ", $_[0]);
	my $missing_rule = $_[1];
	
	for (@data) {
	    $ret .= $tag_line_break."&nbsp;&nbsp;&nbsp;- ";
	    my @subdata = split(" \\| ", $_);
	    my $first = 0;
	    my $subdata = @subdata;
	    
	    for (@subdata) {
		$ret .= " ".gettext("or")." " if $first;

		my $full_info = $_;
		my ($pack) = /^(.*) \(.*$/;
		$pack = $full_info unless $pack;
		
		# FIXME: This method miss the "Provides:"
		if ($package_version{$pack}) {
		    $ret .= "<a href=\"".$pack."_".$package_version{$pack}.".html\">".$full_info."</a>";
		} elsif ($missing_rule ne 'conflicts' && $subdata < 2) {
		    # Print with missing class only if we are not dealing with 
		    # "Conflicts:" or if we have no "or" case.
		    # (FIXME: this last case remain to be thought about)
		    $ret .= "<font class=\"missing\">".$full_info."</font>";
		} else {
		    $ret .= $full_info;
		} 
		$first = 1;
	    }

	    $ret .= ".";	
	}
    }
    return $ret;
}

sub PdbvXhtmlMakePriority {
    my $ret = $_[0];
    if ($_[0] eq 'required') {
	$ret = gettext("required");
    } elsif ($_[0] eq 'important') {
	$ret = gettext("important");
    } elsif ($_[0] eq 'standard') {
	$ret = gettext("standard");
    } elsif ($_[0] eq 'optional') {
	$ret = gettext("optional");
    } elsif ($_[0] eq 'extra') {
	$ret = gettext("extra");
    }
    
    if ($package_explanationurl_priority) {
	return "<a href=\"".$package_explanationurl_priority."\">".$ret."</a>";
    } else {
	return $ret;
    }
}

sub PdbvXhtmlMakeDate {
    my ($year, $month) = split(" ", $_[0]);
    if ($month eq '01') {
	$month = gettext("January");
    } elsif ($month eq '02') {
	$month = gettext("February");
    } elsif ($month eq '03') {
	$month = gettext("March");
    } elsif ($month eq '04') {
	$month = gettext("April");
    } elsif ($month eq '05') {
	$month = gettext("Mai");
    } elsif ($month eq '06') {
	$month = gettext("June");
    } elsif ($month eq '07') {
	$month = gettext("July");
    } elsif ($month eq '08') {
	$month = gettext("August");
    } elsif ($month eq '09') {
	$month = gettext("September");
    } elsif ($month eq '10') {
	$month = gettext("October");
    } elsif ($month eq '11') {
	$month = gettext("November");
    } elsif ($month eq '12') {
	$month = gettext("December");
    } 
    return $year.", ".$month;
}

sub PdbvXhtmlMakeSize {
    my $size = $_[0];
    
    if ($size >= 1048576) {
	$size = (($size / 1048576 * 100) / 100);
	$size = sprintf("%2.f", $size)." ".gettext("GB");
    } elsif ($size >= 1024) {
	$size = (($size / 1024 * 100) / 100);
	$size = sprintf("%2.f", $size)." ".gettext("MB");
    } else {
	$size = sprintf("%2.f", $size)." ".gettext("kB");
    }
    
    return $size;
}


sub PdbvXhtmlPrintSizeBox { 
    # It was planed to make an exact graphical representation, but
    # $percent almost never reach one.
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];

    my $size = PdbvXhtmlMakeSize($_[1]);
    my $percent = (($_[1] / $package_installedsize_sum) * 100);
    my $percent_exagerated = sprintf("%2.f\%", ($percent * 100));
    

    print $handle " ".$size." [ ".$percent.gettext("%")." ]".$tag_line_break;

    unless ($percent_exagerated eq " 0%") {
	print $handle "<table nowrap width=\"100%\" class=\"percentbox\"><tr><td width=\"".$percent_exagerated."\" class=\"percentboxused\">&nbsp;</td><td>&nbsp;</td></tr></table>";
    } else {
	print $handle "<table nowrap width=\"100%\" class=\"percentbox\"><tr><td>&nbsp;</td></tr></table>";
    }
}

sub PdbvXhtmlPrintPopularityBox { 
    my $handle = STDOUT;
    $handle = $_[0] if $_[0];

    if ($_[1] eq gettext("recently installed")) {
	print $handle " ".gettext("This package has been installed or updated recently, we do not have meaningful data.");
    } elsif ($_[1] eq gettext("with no executable files")) {
	print $handle " ".gettext("No executable program has been found in this package, we cannot find meaningful data.");
    } elsif ($_[1] eq gettext("unused")) {
	print $handle $tag_line_break."<table nowrap width=\"100%\" class=\"percentboxused\"><tr><td>".gettext("This package has not been used since more than a month.")."</td></tr></table>";
    } else {
	print $handle $tag_line_break."<table nowrap width=\"100%\" class=\"percentbox\"><tr><td>".gettext("This package seems to be frequently used.")."</td></tr></table>";
    } 
}

sub PdbvXhtmlAddRequiredFiles {
    # Htaccess and css files must be there
    # It could be done in core.pl, but it really depends on the xhtml.

    # Set a pointer to .htaccess, normally empty by default
    unless (-e $working_dir."/.htaccess") {
	symlink($confdir."/htaccess", $working_dir."/.htaccess");
	print "$confdir/htaccess -> $working_dir/.htaccess\n" if $debug;
    }

    # Make sure the appropriate css is there
    unless (readlink($working_dir_css) eq $css) {
	symlink($css, $working_dir_css);
	print "$css -> $working_dir_css\n" if $debug;
    }    
}

# end
