# (c) 2002-2005 Mathieu Roy <yeupou@gnu.org>
# dpkg.pl this file is part of package_db_view
#
#   all dpkg commands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://gna.org/projects/pdbv
#  send comments at <pdbv-dev@gna.org>
#
#  $Id: dpkg.pl,v 1.23 2005/02/23 10:42:20 yeupou Exp $

print "The purpose of this file is not being executed alone";
exit;

# init values for dpkg
$package_info_baseurl = "http://packages.debian.org/";
$package_installedsize_sum;
$package_explanationurl_priority = "http://www.debian.org/doc/debian-policy/ch-archive.html#s-priorities";
our @package_list;


# init packages raw info list
sub PdbvPackageInit {

    {
	open(STATUS, "$dpkg_dir/status") or die $sbst_unable_to_open."$dpkg_dir/status.\n\nStopped";
	local $/ = '';
	while (<STATUS>) {
	    if (/^Package: / ... /^$/) {
		# Figure out the package name
		my ($pack) = /^Package: (.*)$/m;
		my ($version) = /^Version: (.*)$/m;
		push(@package_list, $pack) if $pack && $version;
		next unless $pack && $version;
		
		# Continue if a package name and version was found, 
		#fill the hashes:
	        # FIXME: extracting conffiles apparently does not work
		($package_conffiles{$pack}) = /^Conffiles: (.*)$/sm;
		($package_configversion{$pack}) = /^Config-Version: (.*)$/m;
		($package_conflicts{$pack}) = /^Conflicts: (.*)$/m;
		($package_depends{$pack}) = /^Depends: (.*)$/m;
		($package_description_short{$pack}) = /^Description: (.*)$/m;
		($package_description{$pack}) = /^Description: (.*)$/sm;
		($package_enhances{$pack}) = /^Enhances: (.*)$/m;
		($package_essential{$pack}) = /^Essential: (.*)$/m;
		($package_installedsize{$pack}) = /^Installed-Size: (.*)$/m;
		($package_installdate{$pack}) = PdbvDate($pack);
		($package_maintainer{$pack}) = /^Maintainer: (.*)$/m;
		($package_origin{$pack}) = /^Origin: (.*)$/m;
		($package_priority{$pack}) = /^Priority: (.*)$/m;
		($package_provides{$pack}) = /^Provides: (.*)$/m;
		($package_recommends{$pack}) = /^Recommends: (.*)$/m;
		($package_section{$pack}) = /^Section: (.*)$/m;
		($package_source{$pack}) = /^Source: (.*)$/m;
		($package_status{$pack}) = /^Status: (.*)$/m;
		($package_suggests{$pack}) = /^Suggests: (.*)$/m;
		($package_version{$pack}) = $version;
		($package_website{$pack}) = /^ Homepage: (.*)$/m;

		# Misc formating stuff
		$package_description{$pack} =~ s/\n \..*/\n/g;
		$package_description{$pack} =~ s/^ Homepage: .*$//gm;

		# Whole database installed-size
		$package_installedsize_sum = ($package_installedsize{$pack} + $package_installedsize_sum);

	    }
	}
	close(STATUS);
    }
    @package_list = sort(@package_list);

    # Get pop-con results, if existing
    if (-e $popcon_file) {
	$package_popularity_activated = 1;
	open(POPCON, $popcon_file);
	while (<POPCON>) {
	    my ($atime, $ctime, $pack) = split(" ", $_);
	    if (/\<OLD\>/) {
		$package_popularity{$pack} = gettext("unused");
		next;
	    }
	    if (/\<RECENT-CTIME\>/) {
		$package_popularity{$pack} = gettext("recently installed");
		next;
	    }
	    if (/\<NOFILES\>/) {
		$package_popularity{$pack} = gettext("with no executable files");
		next;
	    }
	    # If we arrived here, there is relevant data
	    $package_popularity{$pack} = gettext("frequently used");
	}
	close(POPCON);
    }

}

# return the dpkg version
sub PdbvPackagemanagerVersion {
    my $ret = `dpkg --version | head -n 1`;
    chomp($ret);
    $ret =~ s/\.$//;
    return $ret;
}

# return the packages list
sub PdbvPackages { 
    return @package_list;
}

sub PdbvPackagesSorted {
    return @package_list;
}

# return package name without the version
sub PdbvBasename {
    my $ret = $_[0];
    $ret =~ s/\_.*$//g;
    return $ret;
}

# return all files in a package
sub PdbvList {
    my $pack = $_[0];
    my $ret = '';
    if (-e "$dpkg_dir/info/$pack.list") {
	open(LIST, "$dpkg_dir/info/$pack.list") or die $sbst_unable_to_open."$dpkg_dir/info/$pack.list.\n\nStopped";
	while (<LIST>) {
	    $ret .= $_;
	}
	close(LIST);
    }
    return $ret;
}

# return installation date (YYYY Month) of the package
# it can be usefull for list by criterion
sub PdbvDate {
    if (-e "$dpkg_dir/info/".$_[0].".list") {
	my $sb = stat("$dpkg_dir/info/".$_[0].".list");
	my ($sec,$min,$hour,$mday,$month,$year,$sday,$aday,$est_dst) = localtime $sb->mtime;
	return ($year+1900)." ".sprintf("%02d", ($month+1) % 100);
    } else {
	return 0;
    }
}
