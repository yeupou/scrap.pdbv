# (c) 2002-2005 Mathieu Roy <yeupou@gnu.org>
# sysinfo.pl: this file is part of package_db_view
#
#   Any system specific values.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#  take a look at http://savannah.nongnu.org/projects/pdbv
#  send comments at <pdbv-dev@nongnu.org>
#
#  $Id: sysinfo.pl,v 1.13 2005/02/23 10:42:20 yeupou Exp $

print "The purpose of this file is not being executed alone";
exit;

# We want to know the computer  with deal with.
# We can access directly /proc/cpuinfo and this kind of files that
# give much informations. But in fact, most of the time, we will use uname
# et caetera. 
# It is more portable, but less informative with older uname.

# Distro version?
sub PdbvSysinfoGetDistro {
    my $hdist = 0;
    my $ret;
    my $debian_rel = "/etc/debian_version";
    my $redhat_rel = "/etc/redhat-release";
    my $url;

    if (-e $redhat_rel) {
	$hdist = $redhat_rel;
    } elsif (-e $debian_rel) {
	$hdist = $debian_rel;
	$url = "http://www.debian.org";
	$ret = gettext("Debian GNU release ");
    }
    
    if ($hdist) {
	open(FILE, $hdist);
	while (<FILE>) {
	    $ret .= $_; 
	}
	close(FILE);
    } else {
	$ret = gettext("unknown distribution");
    }
    
    if ($url) {
	# This is XHTML specific, should be removed if we add another
	# ouput type.
	return "<a href=\"".$url."\">".$ret."</a>";
    } else {
	return $ret;
    }
}

# Kernel?
sub PdbvSysinfoGetUnameKernel {
    my $uname_s = `uname -s 2> /dev/null`;
    my $uname_mr = `uname -mr 2> /dev/null`;
    my $uname_v = `uname -v 2> /dev/null`;
    chomp($uname_s, $uname_mr, $uname_v);
    my $uname_smr = "$uname_s $uname_mr";

    return sprintf(gettext("kernel %s, compiled %s"), $uname_smr, $uname_v);
}

# Cpu?
sub PdbvSysinfoGetUnameCpu {
    # We cannot rely on uname, linux does not provide correct informations
    # with something else than /proc/cpuinfo.
    #
    # So we work on /proc/cpuinfo. If it's too much work to maintain it,
    # we'll drop it.
    #
    # If you get "Processor Unknown" or something inappropriate,
    # you can give us the content of/proc/cpuinfo at
    # http://savannah.nongnu.org/bugs/?func=detailbug&bug_id=3751&group_id=2348
    # or at <pdbv-dev@nongnu.org>.
    # If you system does not have /proc/cpuinfo (hurd), you can tell us
    # how to get these informations.

    my $cpuinfo = "/proc/cpuinfo";
    if (-r $cpuinfo) {
	# Look for raw data, ok for:
	# i*86, powerpc, s390, hppa, alpha, ia64, sparc
	my $smp;
	my $bogomips;
	my $vendor;
	my $model;
	my $mhz;
	my $cache;
	my $bogomips;

	# We need to reset the input separator (set by dpkg.pl)
	my $is = $/;
	$/ = "\n";

	open(CPUINFO, "< $cpuinfo");
	while (<CPUINFO>) {
	    # Number of processors
	    if (m/^(processor(.*: \d| \d)|CPU\d)/i) {
		$smp++;
	    }
	    
	    # Details
	    if (m/^vendor/i) {
		# Ex: GenuineIntel
		s/[vendor|machine].*: (.*)/$1/i;
		chomp;
		$vendor = $_;
	    next;
	    }
	    if (m/^(cpu family)/i && !$vendor) {
		# Ex: PA-RISC 2.0 
		s/cpu family.*: (.*)/$1/i;
		chomp;
		$vendor = $_;
		next;	    
	    }
	    if (m/^(model name|cpu model|family|processor.*: Intel|cpu.*:.*Sparc)/i) {
		# Ex: Intel(R) Pentium(R) 4 CPU 1.70GHz
		#     PCA56 
		#     Itanium
		#     Intel StrongARM-110 rev 3 (v4l) 
		#     TI UltraSparc II (BlackBird) 
		s/^[model name|cpu model|family|processor|cpu].*: (.*)/$1/i;
		chomp;
		$model = $_;
		next;
	    }
	    if (m/^(cpu Mhz|clock)/i) {
		# Ex: 1700.121
		s/^[cpu Mhz|clock].*: (.*)/$1/i;
		chomp;
		$mhz = $_;
		next;
	    }
	    if (m/^bogomips/i) {
		# Ex: 3381.65
		s/^bogomips.*: (.*)/$1/i;
		chomp;
		$bogomips = $_;
		next;
	    }
	}
	close(CPUINFO);
	
	# Get back the input separator
	$/ = $is;

	# There must be at least one CPU
	$smp = 1 unless $smp;
	
	# Prepare the final output
	my $cpudetails;

	if ($vendor|$model) {
	    $cpudetails .= "$vendor " if $vendor;
	    $cpudetails .= "$model " if $model;
	} else {
	    $cpudetails .= gettext("unknown");
	}
	if ($mhz|$bogomips) {
	    $cpudetails .= "(";
	    $cpudetails .= "$mhz MHz, " if $mhz;
	    $cpudetails .= "$bogomips bogomips" if $bogomips;
	    $cpudetails .= ")";
	}
	
	# Print
	return $smp." ".sprintf(gettext("processor %s"), $cpudetails);
    }
}


# Ram, swap?
sub PdbvSysinfoGetFree {
    # Free can be available only if there is a /proc/meminfo
    # If so, we try to use free.
    # (There is little chance that procps is not installed)
    if (-e "/proc/meminfo") {
	my $kb = gettext("kB");
	my $memtotal = `free | grep Mem | awk '{print \$2}'`;
	my $swaptotal = `free | grep Swap | awk '{print \$2}'`;
	chomp($memtotal, $swaptotal);
	
	return sprintf(gettext("%s $kb mem, %s $kb swap"), $memtotal, $swaptotal);
    }
}

# Information about the /usr partition. The result will be used frequently
# so the function frequently used should preferably only return a $var
# defined once.
# NOTE: this stuff is not used right, % relative to the whole installed-size 
# from the database seems more sensible.

my $usr_partition;
my $usr_partition_size;
sub PdbvSysinfoDetermineSystemPartitionInfo {
    # We use as possible df
    open(DF, "df /usr |");
    while (<DF>) {
	my $df;
	($df) = /^(\/.*)$/;
	($usr_partition, $usr_partition_size) = split(" ", $df);
    }
    close(DF);
}

sub PdbvSysinfoGetSystemPartition {
    return $usr_partition;
}

sub PdbvSysinfoGetSystemPartitionSize {
    return $usr_partition_size;
}


# end
